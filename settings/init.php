<?php
/**
 * all initial information, paths, globals	
 * created on 16.02.2007 by Roland Meyer
 */

// $OS = "macosx"
$OS = "linux";


$CWBDIR = "/opt/cwb/cqp/";
$PARCORPUSDIR = "/data/parallel/ParaSol/";
$REGISTRY = "/data/parallel/ParaSol/Registry";

// only relevant for CWB HTML-based concordance
$CQPINIT = "settings/cqpinit";
$HARDBOUNDARY = "999";
$ENCODING = "UTF-8";

// only relevant for XML-based concordance
$ANNOTCONTEXT = "show +tag; show +tag2; show +lemma; set Context 1s;";


$RESOURCES = parse_ini_file("settings/LanguagesAndResources.ini", true);
$LANGUAGES = parse_ini_file("settings/Languages.ini", true);


?>

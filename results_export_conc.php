<?php 
/*
 *  created 01.03.2007 by Roland Meyer
 *  export php for old concordancer (wrapped around CWB html)
 *  faster and perhaps more reliable than XML based version
 */
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"results.csv\""); 
include('settings/init.php');
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}

// Suchparameter auslesen:
$selectedTexts = isset($_POST['selectedTexts']) ? unserialize(stripslashes($_POST['selectedTexts'])) : array();
$selectedLanguages = isset($_POST['selectedLanguages']) ? unserialize(stripslashes($_POST['selectedLanguages'])) : array();
$langs = isset($_POST['langs']) ? unserialize(stripslashes($_POST['langs'])) : array();
$primlang = isset($_POST['primlang']) ? unserialize(stripslashes($_POST['primlang'])) : "";
$query = isset($_POST['query']) ? unserialize(stripslashes(urldecode($_POST['query']))) : array();
$kontextnum = isset($_POST['kontextnum']) ? unserialize(stripslashes($_POST['kontextnum'])) : 10;
$kontexttyp = isset($_POST['kontexttyp']) ? unserialize(stripslashes($_POST['kontexttyp'])) : "word";
$warenkorb = isset($_POST['warenkorb']) ? $_POST['warenkorb'] : array();

// Sucheingabe umsetzen in cqp
$queries = array ();
foreach (array_keys($selectedTexts) as $text) {
	$actquery = 'set Context ' . $kontextnum . ' ' . $kontexttyp . '; ' . strtoupper($text . '_' . $primlang) . '; ';
	foreach ($selectedTexts[$text] as $tlang => $val) {
		if (($tlang != 'all') && ($tlang != $primlang)) {
			$actquery .= 'show +' . strtolower($text . '_' . $tlang) . '; ';
		};
	};
	$utfquery = $actquery . $query[$primlang];
	foreach ($selectedTexts[$text] as $tlang => $val) {
		if (($tlang != 'all') && ($tlang != $primlang) && ($query[$tlang])) {
			$utfquery .= ": " . strtoupper($text) . '_' . strtoupper($tlang) . " " . $query[$tlang];
		};
	};
	$utfquery .= ' ;';
	$queries[$text]['utf'] .= $utfquery;
};

// Array actlangs vorbereiten
$actlangs = $langs;
$primlangpos = array_search($primlang, $actlangs);
array_splice($actlangs, $primlangpos, 1);
array_unshift($actlangs, $primlang); 
ksort($actlangs);

// Suche erneut durchfuehren und nur die ausgewaehlten Treffer abspeichern
$resulttable = array();
$hitno = 0;
foreach ($queries as $text => $query) {
	$hits = array ();
	exec("$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query['utf'] . "'", $hits);
	for ($i = 0; $i < sizeof($hits); $i += sizeof($selectedTexts[$text])) {
		$versatz = 0;		
		for ($j = 0; $j < sizeof($actlangs); $j++) {
			if ($selectedTexts[$text][$actlangs[$j]] == 1) {
					if ($actlangs[$j] == $primlang) {
						preg_match('/^\s*\d*:\s*((.*)\<span .*\>(.*)\<\/span\>(.*))/', $hits[$i + $j], $stelle);
						$resulttable[] = array('PrimLC'=>$stelle[2], 'PrimMatch'=>$stelle[3], 'PrimRC'=>$stelle[4], 'PrimLang'=>$primlang, 'Corpus'=>$text);
					} else {
						preg_match('/^\s*-->(.*)\_(..+):\s*(.*)/', $hits[$i + $j - $versatz], $stelle);
						$resulttable[$hitno][$stelle[2]] = $stelle[3];
					};
			} else { $versatz++; };
		};
		$hitno = $hitno + 1;
	};
};

$mykeys = array_keys($resulttable[0]);
foreach (array('Corpus', 'PrimLang', 'PrimLC', 'PrimMatch', 'PrimRC') as $k){
	echo ($k . "\t");
	$mykeys = array_merge(array_slice($mykeys, 0, array_search($k, $mykeys)), array_slice($mykeys, array_search($k, $mykeys)+1));
};
sort($mykeys);
echo (implode("\t", $mykeys) . "\n");

foreach ($warenkorb as $i) {
	echo ($resulttable[$i]['Corpus']) . "\t";
	echo ($resulttable[$i]['PrimLang']) . "\t";
	echo ($resulttable[$i]['PrimLC']) . "\t";
	echo ($resulttable[$i]['PrimMatch']) . "\t";
	echo ($resulttable[$i]['PrimRC']) . "\t";
	foreach ($mykeys as $k){
		echo ($resulttable[$i][$k]) . "\t";
	};
	echo ("\n");
};
?>

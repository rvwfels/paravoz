<?php 
/*
 * created 01.03.2007 by Roland Meyer
 * provides context functions for the old CWB HTML Based version
 * Since token numbers for aligned languages are unknown, we actually construct a new query using result words.
 */
include('settings/init.php'); 
//$corpus = $_GET["corpus"];
$corpus = preg_replace('/(.*)\_.*/', '$1', $_GET["corpus"]);
$token_nr = $_GET["token_nr"];
$lang = $_GET["lang"];
$primlang = $_GET["primlang"];
 
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}

if ($primlang != $lang){
$query = 'set Context 100 word; '.strtoupper($corpus.'_'.$primlang).'; show +'.$corpus.'_'.$lang.'; Go = []; cat Go '.$token_nr.' '.$token_nr.';';

//echo "$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query. "'";
exec("$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query. "'", $hits);
$r = $hits[1];
$r = str_replace('-->'.$corpus.'_'.$lang.': ',"",$r);
$r_words = explode(" ", $r);


$search_words = '';
foreach ($r_words as $word) {
    $search_words .= ' "'.$word.'"';
}

$search_words = str_replace('"\'"', '[]', $search_words);
$search_words = str_replace('"?""', '[]', $search_words);
$search_words = str_replace('"-""', '[]', $search_words);
$search_words = str_replace('":""', '[]', $search_words);
$search_words = str_replace('"""', '[]', $search_words);
$search_words = str_replace('"?"', '[]', $search_words);
$search_words = str_replace('"]"', '[]', $search_words);
$search_words = str_replace('"["', '[]', $search_words);
$search_words = str_replace('"|"', '[]', $search_words);
$search_words = str_replace('"("', '[]', $search_words);
$search_words = str_replace('")"', '[]', $search_words);
$search_words = str_replace('"-"', '[]', $search_words);



$query2 = strtoupper($corpus.'_'.$lang).';' . $search_words.';';
} else {
$query2 = 'set Context 100 word; '.strtoupper($corpus.'_'.$primlang).'; Go = []; cat Go '.$token_nr.' '.$token_nr.';';
}
$execstring = "$CWBDIR" . "cqpcl -r $PARCORPUSDIR" . "Registry 'set Context 5 s; " . $query2 ."'";
exec($execstring, $out);
$outstr = trim(implode("\n", $out));

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de_DE" xml:lang="de_DE">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body>
<p align="left" style="font-family:Tahoma; font-size:12px; color:black">
<?php print_r($outstr); ?>
</p>
</body>
</html>


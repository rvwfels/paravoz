<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de_DE" xml:lang="de_DE">
<!--
 * created on 10.01.2007 by Roland Meyer
 * rewritten with Ajax components by Andreas Zeman and Ruprecht von Waldenfels 
 * This form basically includes all the elements of the query page. It also uses functions from query_form_objects.php
 * and javascript files from the js folder. Why exactly the functions are distributed in exactly this way remains unknown, but it works.
 * 
-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $ENCODING; ?>" />
	<title><?php echo $PAGE_TITLE; ?></title>
	<link rel="stylesheet" href="<?php echo $CSS_LINK; ?>" type="text/css"></link>
	<link rel="stylesheet" href="css/form.css" type="text/css"></link>
	<script type="text/javascript" src="js/jsfunctions.js"></script>
	<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
</head>
<body>

	 
	 <h3><?php echo $PAGE_HEADLINE; ?></h3>
	 <p> <?php echo $PAGE_CONTENT; ?> </p>


	<?php echo $FORM_DATA; ?>
	
	<?php
		echo '
				<div id="textsform"></div>
				<div id="languageform"></div>
				<br class="clear" />
				
				<input type="submit" class="submit" name="btn[xml]" width="100" value=" Search "/>
				<input type="submit" class="submit" name="btn[xmlfile]" width="100" value=" Export XML "/>
				<input type="submit" class="submit" name="btn[conc]" width="100" value=" old concordancer "/>
				<br />
				</form><br class="clear" />';
	?>
	<script>
// javascript by ?
// 
					// Ajax-Funktionen (verlangen Einbindung von JQuery) 
					 
					 $(document).ready(function() {
					
					 // Toggle-Funktion für Werke -> lädt Daten asynchron aus query_table_of_texts.php
					 $("#togglebooks").live('click', function() {
						var str = $("#corpusform").serialize();
						
						$.post("query_table_of_texts.php?toggle=yes", str, function(data){
							   $("#textsform").html(data);
						   }, "html");
						
						setTimeout (function() {
							$("#togglebooks").attr('checked', false);
							},100);  
					 });
					
					 // Toggle-Funktion für Sprachen 
					 $(".togglelanggroup").click(function() {
						
						var langgroup = $(this).parent().attr("id");
						
						$("#"+langgroup+" input[name=\"langs[]\"]").each(function() {
							var checked = $(this).attr("checked");
							var id = $(this).attr("value");
							var primid = $("input[name=\"primlang\"]:checked").val();
							
							if (id != primid) {
							if (checked == true) {
								$(this).attr("checked", false);
								removelanguage(id);
							} else {
								$(this).attr("checked", true);
								addlanguage(id);
							}
							}
						});
						changetexts();
						setTimeout (function() {
							$(".togglelanggroup").attr('checked', false);
							},100);  
						
					 });
					 
					 $("#togglelang").click(function() {
						$("input[name=\"langs[]\"]").each(function() {
							var checked = $(this).attr("checked");
							var id = $(this).attr("value");
							var primid = $("input[name=\"primlang\"]:checked").val();
							
							if (id != primid) {
							if (checked == true) {
								$(this).attr("checked", false);
								removelanguage(id);
							} else {
								$(this).attr("checked", true);
								addlanguage(id);
							}
							}
						});
						changetexts();
					 });
					 
					 
					
					 
					 // Wenn Primärsprache geändert wird
					 $("input[name=\"primlang\"]").click(function(){
						
						
						var id = $("input[name=\"primlang\"]:checked").val();
						
						var checked = $("#"+id+" > input").attr("checked");
						$("#"+id+" > input").attr("checked", "true");
						
						
						changetexts();
						if (checked==false) {
							addlanguage(id,1);
						} else {
							movelanguage(id);
						}
						
					   });
					
					
					// Hover-Funktion für Primärsprachen -> voller Name
					$(".primlanguage-holder").hover(
						function(){
						$(".primlanguage-holder").css("zIndex","1");
						$(this).css("zIndex","2");
						$(this).css("color","#ff0000");
						var id = $(this).attr("id"); 
						$("#" + id +" > .fullname").css("display","block");
						},
						function(){
						var id = $(this).attr("id"); 
						$("#" + id +" > .fullname").css("display","none");
						$(this).css("color","#000000");
						});
					
					$(".primlanguage-holder > .abbr").click(
						function(){
						var primid = $(this).parent().attr("id");
						var checkedprim = $("#"+primid+" > input").attr("checked");
						
						
						
						if (checkedprim == false) {
							$("#"+primid+" > input").attr("checked", true);
							var id = $("input[name=\"primlang\"]:checked").val();
							var checked = $("#"+id+" > input").attr("checked");
							if (checked==false) {
							$("#"+id+" > input").attr("checked", true);
							addlanguage(id,1);
							} else {
							movelanguage(id);
							}
						changetexts();
						}
						
						});
					
					// Hover-Funktion für Sprachen -> voller Name
					$(".language-holder").hover(
						function(){
						$(".language-holder").css("zIndex","1");
						$(this).css("zIndex","2");
						$(this).css("color","#ff0000");
						var id = $(this).attr("id"); 
						$("#" + id +" > .fullname").css("display","block");
						},
						function(){
						var id = $(this).attr("id"); 
						$("#" + id +" > .fullname").css("display","none");
						$(this).css("color","#000000");
						});
									
					$(".language-holder > .abbr").click(
						function(){
						var id = $(this).parent().attr("id");
						var checked = $("#"+id+" > input").attr("checked");
						
						
						
						if (checked == true) {
							$("#"+id+" > input").attr("checked", false);
							removelanguage(id);
						} else {
							$("#"+id+" > input").attr("checked", true);
							addlanguage(id);
						}
						changetexts();
						});
						
					// Wenn neue Sprache ausgewählt wird
					$("input[name=\"langs[]\"]").click(function(){
							var id = $(this).parent().attr("id");
							var checked = $(this).attr("checked");
							
							
							
							if (checked == true) {
								addlanguage(id);
							} else {
								removelanguage(id);
							}
							changetexts();
					});
					
					$("input[name=\"fullselection\"]").click(function(){
						changetexts();
					});
					
					// Wenn Forumlar abgeschickt wird
					$(".submit").live('click', function(e) {
						 
						 var primlang = $("input[name=\"primlang\"]:checked").val();
						 if (primlang != null) {
							var query = $("input[name=\"query_"+primlang+"\"]").val();
						 }
						 
						 if (primlang == null) {
							alert("<?php echo CORPUS_FORM_4; ?>")
							return false;
						 } else if(query == "") {
							alert("<?php echo CORPUS_FORM_5; ?>")
							return false;
						 } else {			 
							var str = $("form[name=\"tokwic\"]").serialize();
							return true;
						}
					});
				});
					
					// Verschiebe Eingabefeld einer Sprache
					function movelanguage(id) {
						var id;
						var val = $("input[name=\"query_"+id+"\"]").val();
						
						removelanguage(id);
						var lang = $("#"+id+" > .fullname").html();
						
						if (lang == null) {
							lang = $("#"+id+" > .abbr").html();
						}
						$("#languageform > label").removeClass("red");
						
						$("#languageform").prepend("<label for=\"query_"+id+"\" class=\"red\">"+lang+"</label><input name=\"query_"+id+"\" type=\"text\" value=\""+val+"\" /><br />");
					}
						
					
					// Füge Eingabefeld für eine Sprache hinzu
					function addlanguage(id,pos) {
						var id;
					
						var lang = $("#"+id+" > .fullname").html();
						
						if (lang == null) {
							lang = $("#"+id+" > .abbr").html();
						}
						
						if (!pos) {
						$("#languageform").append("<label for=\"query_"+id+"\">"+lang+"</label><input name=\"query_"+id+"\" type=\"text\" value=\"\" /><br />");
						} else {
						$("#languageform > label").removeClass("red");
						$("#languageform").prepend("<label for=\"query_"+id+"\" class=\"red\">"+lang+"</label><input name=\"query_"+id+"\" type=\"text\" value=\"\" /><br />");
						}
					}

					
					// Entferne Eingabefeld einer Sprache
					function removelanguage(id) {
						$("input[name=\"query_"+id+"\"]").next().remove();
						$("input[name=\"query_"+id+"\"]").remove();
						$("label[for=\"query_"+id+"\"]").remove();
					}
					
					// Ändere Werke -> Daten werden aus query_table_of_texts.php gelesen
					function changetexts() {
						var str = $("#corpusform").serialize();
						$.post("query_table_of_texts.php", str, function(data){
							   $("#textsform").html(data);
						   }, "html");
						
					}
					
	</script>
</body>
</html>

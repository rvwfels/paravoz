// includes functions for the language choice components on the query page
var sourceLang // enthält die aktuelle source language

function setSourceLang (f) {
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){
			sourceLang = f.primlang[j].name
		}
	}
}

function MakeArray(n){
   for (var i=0; i<n; i++)
      this[i] = 0
   this.length = n
}

function syncPrimlang(f,t){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
		if(selLangs[i].value == t){
			if(selLangs[i].checked == false){ f.primlang[i].checked = false }
		}
	};
    chkForm(f)
}

function markLang(f){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
			if(f.primlang[i].checked == true){ selLangs[i].checked = true }
	};
    chkForm(f)
}

function chkForm (f) {
	var ok1 = false
	var ok2 = false
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){	ok1 = true }
	}
	for(var i=0;i<selLangslaenge;i++) { 
		if((selLangs[i].checked)&&(!f.primlang[i].checked)){ ok2 = true }
	}	
//	if(ok1 && ok2){ document.forms["languages"].submit() } else {
	if(ok1 && ok2){ return true } else {
		alert('Please select a source language and at least one target language!')
		f.focus()
	}
}

function chkAll(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = true
	}
}

function chkNone(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = false
	}
}

function Go (select) {
	var wert = select.options[select.options.selectedIndex].value;
	if (wert == "leer") {
    	select.form.reset();
	    parent.frames["kwic"].focus();
    	return;
	} else {
		parent.frames["kwic"].location.href = wert;
		select.form.reset();
		parent.frames["kwic"].focus();
    }
}

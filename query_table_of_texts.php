<?php
/*
 * created by Roland Meyer
 * revised by Andreas Zeman and Ruprecht von Waldenfels
 * this class lists the texts that can be chosen in a table on the lower left
 */
error_reporting(0);


include ('settings/init.php');
include ("query_form_objects.php");
$regTbl = new corpusTable("$REGISTRY");

// liest die verfügbaren Werke asynchron aus

if (isset($_GET["toggle"])) {
	$toggle = true;
} else {
	$toggle = false;
}


function checked ($array, $lang, $toggle=false) {
	if ($toggle==false) {
		return ' checked="checked"';
	} else {
		if (in_array($lang,$_POST["$array"])) {
			return '';
		} else {
			return ' checked="checked"';
		}
	}
}

function checked_for_allfield ($array, $lang, $toggle=false) {
	if ($toggle==false) {
		return true;
	} else {
		if (in_array($lang,$_POST["$array"])) {
			return false;
		} else {
			return true;
		}
	}
}

function allchecked($allchecked) {
		if ($allchecked) {
			return ' checked="checked"';
			} else {
			return '';
			}		
	}

if ($_POST['primlang']) {
	$primlang = $_POST['primlang'];
} else {
	$primlang = 'de';
	};

if ($_POST['langs']) {
	$langs = $_POST['langs'];
} else {
	$langs = array ();
};

$acttexts = array (); // dictionary of relevant texts
$fullselection = $_POST['fullselection'] ? $_POST['fullselection'] : "no";

if ($fullselection == "yes") {
	foreach (array_keys($regTbl->texts) as $text) {
		if ($regTbl->korptabByLang[$primlang][$text] == 1) {
			$acttexts[$text] = 1;
		};
	};
} else {
	foreach (array_keys($regTbl->texts) as $text) {
		$found = 1;
		foreach ($langs as $lang) {
			if ($regTbl->korptabByLang[$lang][$text] != 1) {
				$found = 0;
			};
		};
		if (($found == 1) && ($regTbl->korptabByLang[$primlang][$text] == 1)){
			$acttexts[$text] = 1;
		};
	};
};



echo ('<table border="1"><tr><td><input type="radio" id="togglebooks" name="togglebooks" value="" /></td>');


foreach ($langs as $lang) {
	if ($primlang == $lang) {
		echo ('<td><span class="red">' .
		$lang . '</span></td>');
	} else {
		echo ('<td>' . $lang . '</td>');
	};
};
echo ('</tr>');

foreach (array_keys($acttexts) as $schluessel) {
	$books_in_langs = '';
	$allchecked = true;
	foreach ($langs as $lang) {
		if ($regTbl->korptabByLang[$lang][$schluessel] == 1) {
			$books_in_langs .= '<td title="'. $RESOURCES[strtolower($schluessel . '_' . $lang)]['kurztitel'] .'"><input type="checkbox" class="checkbox" class="selText" name="selText_' .
			$schluessel . '[]"  value="' . $lang . '"'.checked ('selText_'.$schluessel, $lang, $toggle).' /></td>';
			
			if(!checked ('selText_'.$schluessel, $lang, $toggle)) {
					$allchecked = false;
			}
			
			
//			$schluessel . '[]"  value="' . $lang . '" checked="true" /></td>');
		} else {
			$books_in_langs .= '<td></td>';
		};
	};
	
	
	echo ('<tr><td title="' . $RESOURCES[strtolower($schluessel . '_' . $lang)]['origtitel'] . '"><input type="checkbox" class="checkbox" name="selAll"');
	echo ('  value="all"'.allchecked($allchecked).' onChange="if(!checked){
					chkNone(\'selText_' . $schluessel . '[]\')} else {
					chkAll(\'selText_' . $schluessel . '[]\') }"/><a href="http://www-korpus.uni-r.de/ParaSol/table.html#'.$schluessel.'" target="_blank">' . $schluessel . '</a></td>');
	echo $books_in_langs;
	echo ('</tr>');
};

/**
 * query lines
 */
echo ('</table>');
foreach (array_keys($acttexts) as $x) {
	echo ('<input type="hidden" name="acttexts[]" value="' . $x . '"/>');
};
?>

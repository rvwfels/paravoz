<?php

/**
 * class for constructing a corpus table and generating a query form
 * created on 16.02.2007 by Roland Meyer
 * function getForm() rewritten by Andreas Zeman and Ruprecht von Waldenfels
 */
			
class corpusTable {
/**
 * @class corpusTable
 */

/**  
 * path to the CWB registry directory
 * @var string $registry
 */ 
	var $registry;
/** 
 *  hash of hashes of the form 'text => [lang_1, ..., lang_n]', sorted by keys
 *	representing a CWB parallel corpus
 * @var array $korptab
*/
	var $korptabByText = array();

/** 
 *  hash of hashes of the form 'lang => [text_1, ..., text_n]', sorted by keys and values
 *	representing a CWB parallel corpus
 * @var array $korptabByLang
*/
	var $korptabByLang = array();

/**
 * array of arrays containing the language shortnames and longnames used in the corpus
 * @var array $init_languages
 */
	var $languages = array();
	
/**
 * array of arrays containing the text shortnames and longnames used in the corpus
 * @var array $texts
 */
	var $texts = array();



/**
 * constructor 
 * @method corpusTable
 * @param string registry 
 */ 
	function corpusTable ($registry){
		$this->registry = $registry;
		$this->parseRegDirectory();
	}
	
/**
 *  Parses a given CWB registry directory into $korptab  
 *  Achtung auf Varianten: letzer Kleinbuchstabe 
 */ 
	function parseRegDirectory(){
		$handle=opendir($this->registry); 
		while ($file = readdir ($handle)) {
			preg_match("/^([a-zA-Z0-9]+)\_([a-zA-Z0-9]{0,3})$/", $file, $treffer);
			if ($treffer[2]) { 
				$this->korptabByText[$treffer[1]][$treffer[2]] = 1;	
				$this->korptabByLang[$treffer[2]][$treffer[1]] = 1;
				$this->languages[$treffer[2]] = 1;
				$this->texts[$treffer[1]] = 1;			
			}
		}
		foreach (array_keys($this->languages) as $schluessel1){
			foreach (array_keys($this->korptabByText) as $schluessel2){
				if ( $this->korptabByText[$schluessel2][$schluessel1] != 1){
					 $this->korptabByText[$schluessel2][$schluessel1] = 0;
				}	
			}
		}
		foreach (array_keys($this->texts) as $schluessel1){
			foreach (array_keys($this->korptabByLang) as $schluessel2){
				if ( $this->korptabByLang[$schluessel2][$schluessel1] != 1){
					 $this->korptabByLang[$schluessel2][$schluessel1] = 0;
				}	
			}
		}	
	}	
	
/**
 * dumps out form data
 * clicking the radio button automatically selects the checkbox of the respective lang
 * clicking the checkboxes automatically adds/retracts available radiobuttons
 * @return string containing the full html form generated from the corpusTable
 */ 
	function getForm($name, $target){
		global $LANGUAGES;
// get the posted values
		if($_POST['primlang']){ $primlang = $_POST['primlang']; };
		if($_POST['langs']){ $langs = $_POST['langs']; } else { $langs = array( NULL => '');};
		$fullselection = $_POST['fullselection'] ? $_POST['fullselection'] : "no"; 
		

// add to the form data	
		$formData = '<form id="corpusform" name="tokwic" action="results.php" method="POST" target="_blank">';

/**
 *  primary language
 */
		$formData .= '<label for="primlang">Primary language: </label>';
		$index = 0;
		
		$keys = array_keys($LANGUAGES);
		$init_languages = array();
		
		$langs = array();
		

		foreach ($keys as $key) {
			
			$temparray = array();
			$parts = preg_split("/_/", $key);
			$langs["$parts[1]"] = "1";

			if (!(array_key_exists($parts[0],$init_languages))) {
				$temparray["$parts[1]"] = $LANGUAGES["$key"]["name"];		
				$init_languages["$parts[0]"] = $temparray;
			} 
			else {
				$init_languages["$parts[0]"]["$parts[1]"] = $LANGUAGES["$key"]["name"];	
			}

	

		}
		
		$languages_registry = $this->languages;
		$languages_not_in_db = array_diff_key($languages_registry, $langs);
		
		

		foreach ($init_languages as $groupname=>$grouplanguages) {
		 $formData .= '<div class="languages"><strong>'.ucfirst($groupname).'</strong><br />';
		 $block_nr = 4;
		 $nr_of_languages = count($grouplanguages);
		 $count_languages = 0;		 
		foreach ($grouplanguages as $langabbr=>$langname) {
		 $count_languages = $count_languages+1;
		 if ($block_nr==4) {
		  $formData .= '<div class="languageselection">';
		 }
		 $block_nr = $block_nr-1;
		 $formData .= '<span id="prim-'.$langabbr.'" class="primlanguage-holder"><input class="radio" type="radio" name="primlang" value="'.$langabbr.'" alt="'.$langname.'" /><span class="abbr">'.strtoupper($langabbr).'</span><span class="fullname">'.$langname.'</span></span>';
		 if ($block_nr==0 OR $nr_of_languages==$count_languages) {
				$formData .= '</div>';
				$block_nr = 4;
				} else {
				$formData .= '<br />';
				}
		 }
		 $formData .= '</div>';
		}

		if (count($languages_not_in_db)>0) {
			$formData .= '<div class="languages"><strong>'.CORPUS_FORM_3.'</strong><br />';
			
			$block_nr = 4;
			$nr_of_languages = count($languages_not_in_db);
			$count_languages = 0;
			foreach ($languages_not_in_db as $language_abbr=>$language_id) {
				$count_languages = $count_languages+1;
				if ($block_nr==4) {
					$formData .= '<div class="languageselection">';
				}
				$block_nr = $block_nr-1;
				$formData .= '<span id="prim-'.$language_abbr.'" class="primlanguage-holder"><input class="radio" type="radio" name="primlang" value="'.$language_abbr.'" alt="'.$language_abbr.'" /><span class="abbr">'.strtoupper($language_abbr).'</span></span>';
				if ($block_nr==0 OR $nr_of_languages==$count_languages) {
				$formData .= '</div>';
				$block_nr = 4;
				} else {
				$formData .= '<br />';
				}
			}
						
			
			$formData .= '</div>';
			}
		

/**
 *  languages
 */
		$formData .= '<br /><label for="languages">Aligned languages: </label>';
		$index = 0;
		
		foreach ($init_languages as $groupname=>$grouplanguages) {
			$formData .= '<div class="languages" id="'.ucfirst($groupname).'"><input type="radio" name="togglelanggroup" class="togglelanggroup" value="'.ucfirst($groupname).'" /><span style="float:left; margin:0px; padding:0px;"><strong>'.ucfirst($groupname).'</strong></span><br />';
			
			$block_nr = 4;
			$nr_of_languages = count($grouplanguages);
			$count_languages = 0;
			foreach ($grouplanguages as $langabbr=>$langname) {
		 		$count_languages = $count_languages+1;

			if ($block_nr==4) {
				$formData .= '<div class="languageselection">';
				}
				$block_nr = $block_nr-1;
				$formData .= '<span id="'.$langabbr.'" class="language-holder"><input class="checkbox" type="checkbox" name="langs[]" value="'.$langabbr.'" alt="'.$langname.'" /><span class="abbr">'.strtoupper($langabbr).'</span><span class="fullname">'.$langname.'</span></span>';
				if ($block_nr==0 OR $nr_of_languages==$count_languages) {
				$formData .= '</div>';
				$block_nr = 4;
				} else {
				$formData .= '<br />';
				}
			}
						
			
			$formData .= '</div>';
			

		}
		if (count($languages_not_in_db)>0) {
			$formData .= '<div class="languages"><strong>'.CORPUS_FORM_3.'</strong><br />';
			
			$block_nr = 4;
			$nr_of_languages = count($languages_not_in_db);
			$count_languages = 0;
			foreach ($languages_not_in_db as $language_abbr=>$language_id) {
				$count_languages = $count_languages+1;
				if ($block_nr==4) {
					$formData .= '<div class="languageselection">';
				}
				$block_nr = $block_nr-1;
				$formData .= '<span id="'.$language_abbr.'" class="language-holder"><input class="checkbox" type="checkbox" name="langs[]" value="'.$language_abbr.'" alt="'.$language_abbr.'" /><span class="abbr">'.strtoupper($language_abbr).'</span></span>';
				if ($block_nr==0 OR $nr_of_languages==$count_languages) {
				$formData .= '</div>';
				$block_nr = 4;
				} else {
				$formData .= '<br />';
				}
			}
						
			
			$formData .= '</div>';
			}
				
		
		$formData .= '<br /><br />
					  <label for="fullselection"></label><input type="radio" class="radio" name="fullselection" value="yes" checked="checked" /><span class="radio">All texts</span><input type="radio" class="radio" name="fullselection" value="no" /><span class="radio">Only texts available in all languages</span><span style="margin-left:15px;"><a href="../helpwiki/" target="_blank">Get help</a></span><br />';	
		

/**
 *  full-selection-radiobutton
 */
 
					
/**
 *  retrieve-texts-button
 */ 		
		$formData .= '<br /><br style="clear:both;" />';
		return $formData;	
	}
}

?>

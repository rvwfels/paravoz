<?php 
/*
 * created by Andreas Zeman and Ruprecht von Waldenfels
 * provides wider context for XML-based results window
 * Since token numbers for aligned languages are unknown, we actually construct a new query using result words.
 */
	include('settings/init.php'); 
$corpus = substr($_GET["corpus"],0,-1);
$token_nr = $_GET["token_nr"];
$lang = $_GET["lang"];
$primlang = $_GET["primlang"];
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}

if ($primlang != $lang){
$query = 'set Context 100 word; '.strtoupper($corpus.'_'.$primlang).'; show +'.$corpus.'_'.$lang.'; Go = []; cat Go '.$token_nr.' '.$token_nr.'';

//echo "$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query. "'";
exec("$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query. "'", $hits);
$r = $hits[1];
$r = str_replace('-->'.$corpus.'_'.$lang.': ',"",$r);
$r_words = explode(" ", $r);


$search_words = '';
$count_words = 0;
foreach ($r_words as $word) {
	$count_words++;
	if ($count_words < 100) {
	$search_words .= ' "'.$word.'"';
	}
}

$search_words = str_replace('"\'"', '[]', $search_words);
$search_words = str_replace('"?""', '[]', $search_words);
$search_words = str_replace('"-""', '[]', $search_words);
$search_words = str_replace('":""', '[]', $search_words);
$search_words = str_replace('"""', '[]', $search_words);
$search_words = str_replace('"?"', '[]', $search_words);
$search_words = str_replace('"]"', '[]', $search_words);
$search_words = str_replace('"["', '[]', $search_words);
$search_words = str_replace('"|"', '[]', $search_words);
$search_words = str_replace('"("', '[]', $search_words);
$search_words = str_replace('")"', '[]', $search_words);
$search_words = str_replace('"-"', '[]', $search_words);



$query2 = strtoupper($corpus.'_'.$lang).'; show +tag; show +tag2; show +lemma;  '.$search_words.';';
} else {
$query2 = 'set Context 5s; '.strtoupper($corpus.'_'.$primlang).'; show +tag; show +tag2; show +lemma; Go = []; cat Go '.$token_nr.' '.$token_nr.'';
}

$execstring = "$CWBDIR" . "cqpcl -r $PARCORPUSDIR" . "Registry 'set Context 5s; set PrintMode sgml; " . $query2;
	//delete s-id

if ($OS == "linux") {
  
    $execstring .= "'";
    $execstring .= " | sed -r 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
    $execstring .= " | sed -r 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
    $execstring .= " | sed -r 's/\/__UNDEF__//g'";
    $execstring .= " | sed -r 's/&lt;TOKEN&gt;/<TOKEN>/g'";
    $execstring .= " | sed -r 's/&lt;\/TOKEN&gt;/<\/TOKEN>/g'";
    $execstring .= " | sed -r 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
    $execstring .= " | sed -r 's/&lt;CONTENT&gt;(.*)&lt;\/CONTENT&gt;/<CONTENT>\\1<\/CONTENT>/g'";
    $execstring .= " | sed -r 's/<attribute[^>]+>//g'";
    $execstring .= " | sed -r 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
   
} elseif ($OS = "macosx") {   


    $execstring .= "' | sed -E 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
    $execstring .= " | sed -E 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
    $execstring .= " | sed -E 's/\/__UNDEF__//g'";
    $execstring .= " | sed -E 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
    $execstring .= " | sed -E 's/<attribute[^>]+>//g'";
    $execstring .= " | sed -E 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
    $execstring .= "\n";
}

exec("$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry '" . $query2. "'", $hits2);
	exec($execstring, $out);
$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS corpus="'.$corpus.'_'.$lang.'">'.$outstr ;
$outstr .= '</RESULTS>';
header('Content-type: text/xml; charset=utf-8'); 
echo(trim('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./parallel-export.xsl" ?>'));				
echo ($outstr);

?>

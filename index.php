<?php
/** 
 * corpus selection form 
 * this is the main entry form; buttons below either lead to XML-based or old HTML-based result pages
 *
 * created on 16.02.2007
 * by Roland Meyer
 * revised by Andreas Zeman and Ruprecht von Waldenfels
 */

error_reporting(0);

define('CORPUS_FORM_0', 'Primary language');
define('CORPUS_FORM_1', 'Aligned languages');
define('CORPUS_FORM_2', 'Choose a language...');
define('CORPUS_FORM_3', 'No group');
define('CORPUS_FORM_4', 'Please choose a primary language.');
define('CORPUS_FORM_5', 'Please fill in a query for your primary language.');
	
/** initial specifications */
include ('settings/init.php');


$PAGE_TITLE = "Query interface";
$PAGE_HEADLINE = "Query interface";
$PAGE_CONTENT = 'Choose primary and aligned language(s), and enter a query. You need to define a query for the primary language (in red). In addition, you may define queries on the aligned languages, which will restrict output accordingly. <br><br>';

/** query form is distributed between two files */
include ("query_form_objects.php");
$regTbl = new corpusTable("$REGISTRY");
$FORM_DATA = $regTbl->getForm("languages", "index.php");
include ('query_form.php');


/** submits to results.php */

?>

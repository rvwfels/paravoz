<?php 
/*
 * created on 16.02.2007 by Roland Meyer
 * This is the old concordance mechanism which simply wraps some code via css styles around the HTML output of CWB 
 */
	include('settings/init.php'); 
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de_DE" xml:lang="de_DE">

<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $ENCODING; ?>" />
	<title><?php echo $PAGE_TITLE; ?></title>
    <link rel="stylesheet" href="./css/reactstyle.css" type="text/css"></link> 
<!--	<link rel="stylesheet" href="<?php echo $CSS_LINK; ?>" type="text/css"></link> -->
	<script type="text/javascript" src="js/jsfunctions.js"></script>
</head>	
<body>
<?php
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}

// Sucheingabe umsetzen in cqp
$queries = array ();
foreach (array_keys($selectedTexts) as $text) {
	$actquery = 'set Context ' . $kontextnum . ' ' . $kontexttyp . '; ' . strtoupper($text . '_' . $primlang) . '; ';
	foreach ($selectedTexts[$text] as $tlang => $val) {
		if (($tlang != 'all') && ($tlang != $primlang)) {
			$actquery .= 'show +' . strtolower($text . '_' . $tlang) . '; ';
		};
	};
	$utfquery = $actquery . $query[$primlang];
	foreach ($selectedTexts[$text] as $tlang => $val) {
		if (($tlang != 'all') && ($tlang != $primlang) && ($query[$tlang])) {
			$utfquery .= ": " . strtoupper($text) . '_' . strtoupper($tlang) . " " . $query[$tlang];
		};
	};
	$utfquery .= ' ;';
	$queries[$text]['utf'] .= $utfquery;
};

// Ausgabe
$actlangs = $langs;
$primlangpos = array_search($primlang, $actlangs);
array_splice($actlangs, $primlangpos, 1);
array_unshift($actlangs, $primlang); 
ksort($actlangs);

$hitno = 0;
echo ('<form name="einkauf" action="results_export_conc.php" method="POST" target="_blank">');
// Suchparameter weiterschleifen:
echo ('<input type="hidden" name="selectedTexts" value="'.htmlentities(serialize($selectedTexts)).'"/>');
echo ('<input type="hidden" name="selectedLanguages"  value="'.htmlentities(serialize($selectedLanguages)).'"/>');
echo ('<input type="hidden" name="langs" value="'.htmlentities(serialize($langs)).'"/>');
echo ('<input type="hidden" name="primlang" value="'.htmlentities(serialize($primlang)).'"/>');
echo ('<input type="hidden" name="query" value="'.urlencode(serialize($query)).'"/>');
echo ('<input type="hidden" name="kontextnum" value="'.htmlentities(serialize($kontextnum)).'"/>');
echo ('<input type="hidden" name="kontexttyp" value="'.htmlentities(serialize($kontexttyp)).'"/>');
//
echo ('<input type="submit" name="export" width="100" value=" Export Selected "/>');
echo('<div id="kwic-output">');
echo '<table id="ausgabetabelle">';
foreach ($queries as $text => $query) {
	$hits = array ();
//    $query['utf'] .= "show +tag; show +tag2; " . $query['utf'];
	exec("$CWBDIR" . "cqpcl$CQPOPTIONS -r $PARCORPUSDIR" . "Registry  '" . $query['utf'] . "'", $hits);
	echo ('<tr><td colspan=' . sizeof($actlangs) . '></td></tr>');
	echo ('<tr><td colspan=' . sizeof($actlangs) . '></td></tr>');
	echo ('<tr><td colspan=' . sizeof($actlangs) . '>');
	echo ('<a name="' . $text . '">Corpus </a>' . $text . ':' . '<br/>'); // ??? <a>
	echo ('CQL Query: ' . $query['utf'] . '<br/>');
	echo ('</td></tr>');

	$cellwidth = round(90 / (sizeof($actlangs) + 1), 0);
	for ($i = 0; $i < sizeof($hits); $i += sizeof($selectedTexts[$text])) {
		if ($i % (2 * sizeof($selectedTexts[$text]))) {
			echo '<tr class="gerade">';
		} else {
			echo '<tr class="ungerade">';
		};
		echo '<td width="1%"><input type="checkbox" name="warenkorb[]" value="' . $hitno . '"></td>';
		$versatz = 0;
        $token_nr = 1;	
		for ($j = 0; $j < sizeof($actlangs); $j++) {
			echo ('<td width = "' . $cellwidth . '%">');
			if ($selectedTexts[$text][$actlangs[$j]] == 1) {
					if ($actlangs[$j] == $primlang) {
						preg_match('/^\s*(\d*):\s*((.*)<span .*>(.*)<\/span>(.*))/', $hits[$i + $j], $stelle);
                        $token_nr = $stelle[1];
						echo ($stelle[2]);
						$_SESSION['resulttable'][] = array('PrimLC'=>$stelle[2], 'PrimMatch'=>$stelle[3], 'PrimRC'=>$stelle[4], 'PrimLang'=>$primlang, 'Corpus'=>$text);
					} else {
						preg_match('/^\s*-->(.*)\_(..+):\s*(.*)/', $hits[$i + $j - $versatz], $stelle);
						echo ($stelle[3]);
						$_SESSION['resulttable'][$hitno][$actlangs[$j]] = $stelle[3];
					};
			} else { $versatz++; };
//            print_r($text+'<br/>'+$selectedTexts[$text]+'<br/>');
			echo (' <a href="results_context_conc.php?corpus=' . $text . '_' . $actlangs[$j] . '&amp;token_nr=' . $token_nr .'&amp;primlang=' . $primlang . '&amp;lang=' . $actlangs[$j] . '" title="Show context" class="showcontext" target="_blank">Show context</a></td>');
		};
		echo '</tr>';
		$hitno = $hitno + 1;
	};
};
echo '</table></form>';
?>
</div>
</div>
</body>
